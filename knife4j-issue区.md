![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/203109_56841701_1899542.png "logo-活动主页.png")  
项目地址：https://gitee.com/xiaoym/knife4j  
#### 活动流程
- 参与 issue 解决 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在issue 列表的新手区/其他 issue 区选择一个或多个 issue 任务，完成它。
2. 完成 issue 描述需求后，在 issue 所属仓库中提交 Pull Request ，并复制 issue ID 至 PR 描述，提交审核（注意：提交 pr 时请备注【Gitee八周年活动】）   
 :point_right: [Fork + PullRequest 模式帮助文档](https://gitee.com/help/categories/42)    
 **注：** 特殊 issue（无需提交至仓库的 issue）请和项目维护者沟通后按指定方式提交。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励 Gitee 大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")      
 

#### 新手区 issue 
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

| 编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|1|新增功能|增加JSR303在页面中的信息展示| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I3NQE3) | 
|2|bug|RequestBody类型为Object时,没有body输入框| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I2WCAS) | 
|3|bug|SpringBoot使用WebApplicationType.REACTIVE模式下一些问题| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I2EFVP) | 
|4|bug|Knife4jAggregation Cloud模式聚合问题，服务重启之后，重启服务swagger无法加载接口信息| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I2D6C3) | 
|5|新增功能|ApiOperationSupport的ignoreParameters参数，是否可以支持正则表达式| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I21ZKC) |  


#### 其他 issue 
难度：:fa-star:  :fa-star:  :fa-star-o: 
| 编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|  
|6|新增功能|支持Swagger2规范中的allof特性，针对Go语言中非泛型类型的支持| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I1M6E4) | 
|7|新增功能|适配Spring的hateoas功能| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I23DDX) | 
|8|bug|读取缓存数据是异步操作导致AfterScript 不能设置多个参数| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I3OJUW) | 
|9|新增功能|nutz框架的支持| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I3D9F7) | 
|10|bug|返回结果定义三层，内存飙升并且页面卡死| [查看详情](https://gitee.com/xiaoym/knife4j/issues/I2VRD5) | 

[查看更多可参与的项目>>](https://gitee.com/gitee-community/gitee-8th-participate-in-open-source)

#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png")   
