![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/202718_8c8255e5_1899542.png "主页-logo.png")  
项目地址：https://gitee.com/dolphinscheduler/DolphinScheduler
#### 活动流程
- 完成 issue 任务 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在issue 列表的新手区/其他 issue 区选择一个或多个 issue 任务，完成它。
2. 完成 issue 描述需求后，在 issue 所属仓库中提交 Pull Request ，并复制 issue ID 至 PR 描述，提交审核（注意：提交 pr 时请备注【Gitee八周年活动】）   
 :point_right: [Fork + PullRequest 模式帮助文档](https://gitee.com/help/categories/42)    
 **注：** 特殊 issue（无需提交至仓库的 issue）请和项目维护者沟通后按指定方式提交。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励 Gitee 大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")       

#### 新手区 issue 
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|1|英文视频录制 |单机部署英文视频录制| [查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3RO0H ) | 
|2|文档|文档：中英文文档纠错| [查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3ROEO) |
|3|改进|部分表设计，字段长度不规范。需要进行更正。建议对核心表进行扫描。修改其长度。|[查看详情]|(https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3ROCU)| 
|4|文档|目前Alert-Http插件的几个参数，没有详细的文档描述，用户很难正确使用，因此需要提供相关参数文档。如能提供最佳实践更好。比如采用alert-http来实现短信告警、电话告警等的文档描述。|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3RO5D)| 
|5|文档|系统参数整理，代码中有一些直接使用System.getProperty()获取参数值的，需要将这些整理成文档。|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3RO2Y)| 
  
#### 其他 issue   
难度：:fa-star:  :fa-star:  :fa-star-o: 
|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|
|6|新增功能|工作流实例可跳转到任务实例|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3RODW)| 
|7|新增功能|【前端】告警组列表接口拆分。需要将告警组列表接口拆分 提供详情和列表两个接口|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3ROBM)| 
|8|提升|【后端】告警组接口拆分。需要将列表接口拆分，提供详情接口和列表接口|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3ROAS)| 
|9|改进|alert-script目前执行是dolphinscheduler这个租户，用户权限比较高，因此需要用户配置相关租户，执行script的时候需要切换到这个租户。（通常这个租户权限不会很大）|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3RO4A)| 
|10|英文视频录制|参考本文档，完成k8s 部署英文视频教程|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3RO19)| 
|11|功能|我们知道，在升级到警报插件版本之前，用户通过alert.properties文件通过配置电子邮件，wehcat，dingtalk使用警报。升级到警报插件版本后，用户可以通过在页面上创建警报组，然后创建警报插件的实例来配置电子邮件，wehcat，dingtalk信息。因此，我们应该提供一种工具，当用户升级到警报插件版本时自动扫描用户alert.properties文件中的警报配置，然后自动创建默认警报组和警报插件实例，并将警报组与工作流相关联。|[查看详情](https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I3ROMP)| 


#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png")   