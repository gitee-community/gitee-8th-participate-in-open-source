![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/203312_e6f50287_1899542.png "主页-logo.png")  
项目地址：https://gitee.com/rtthread/rt-thread  
#### 活动流程
- 参与 issue 解决 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在issue 列表的新手区/其他 issue 区选择一个或多个 issue 任务，完成它。
2. 完成 issue 描述需求后，在 issue 所属仓库中提交 Pull Request ，并复制 issue ID 至 PR 描述，提交审核（注意：提交 pr 时请备注【Gitee八周年活动】）   
 :point_right: [Fork + PullRequest 模式帮助文档](https://gitee.com/help/categories/42)    
 **注：** 特殊 issue（无需提交至仓库的 issue）请和项目维护者沟通后按指定方式提交。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励 Gitee 大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")     


#### 新手区 issue 
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|1|优化功能|netdev 的 status_callback 函数的完善| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RHGA) | 
|2|优化功能|netdev 的网卡自动切换功能| [查看详情]( https://gitee.com/rtthread/rt-thread/issues/I3RIYO) | 
|3|优化功能|【网络】AT socket 支持 listen 与 accept 函数的实现| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RIYN) | 
|4|bug|【问题】PWM配置使用存在问题| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RJJD) | 
|5|优化功能|【驱动完善】优化完善DAC相关功能| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RJAY) | 
|6|优化功能|【驱动完善】优化完善ADC相关功能| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RJAT) | 
  

#### 其他 issue 
难度：:fa-star:  :fa-star:  :fa-star-o: 
| 编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|  
|7|优化功能|wiznet 的 socket 相关接口的完善，包括申请的 Socket 类型及对应的属性设置| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RHIY) | 
|8|优化功能|AT 组件对模块 URC 数据的优化| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RHJR) | 
|9|bug|打开 posix 接口时，当真实的 socket 数量超过了 lwip 配置得 socket 数据时引起的系统死机问题；| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I37RMY) | 
|10|bug|【问题】RTC 驱动架构优化| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RJAO) | 
|11|新增功能|【内存管理】支持精细化的多内存块分配管理功能| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RJA4) | 
|12|新增功能|【驱动支持】支持SPI从设备驱动| [查看详情](https://gitee.com/rtthread/rt-thread/issues/I3RJ9X) |     

[查看更多可参与的项目>>](https://gitee.com/gitee-community/gitee-8th-participate-in-open-source)

#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png")    
