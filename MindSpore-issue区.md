![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/204904_3495b283_1899542.png "logo-居左.png")  
项目地址：Https://gitee.com/mindspore. 
#### 活动流程
- 参与 issue 解决 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在issue 列表的新手区/其他 issue 区选择一个或多个 issue 任务，完成它。
2. 完成 issue 描述需求后，在 issue 所属仓库中提交 Pull Request ，并复制 issue ID 至 PR 描述，提交审核（注意：提交 pr 时请备注【Gitee八周年活动】）   
 :point_right: [Fork + PullRequest 模式帮助文档](https://gitee.com/help/categories/42)    
 **注：** 特殊 issue（无需提交至仓库的 issue）请和项目维护者沟通后按指定方式提交。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励 Gitee 大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")      

#### 新手区 issue 
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|1|环境搭建类 |如何使用conda环境中安装的cuda|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3EQVE) | 
|2|文档类|retinanet模型的ms版本说明有问题：在1.2分支上的retinanet模型说明上，说是用1.0.1版本跑的，但在1.0.1tag上没找到这个模型|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3DUPQ) | 
|3|文档类 |Migrate Label.md to Mindspore main repo：迁移社区issue标签说明文档到mindspore主仓并按照当前社区issue清单进行完善|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3OUFK?from=project-issue) | 
|4|文档类|ms.dataset.vision.py_transforms.Pad() API说明错误，须根据API实际代码进行修改|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3S0D5) | 
|5|功能完善类 |Add Minimum Operator Export ONNX：MindSpore模型导出转换成ONNX模型，具体实现细节可拆分为单个MindSpore算子到单个ONNX算子的转换，本任务是实现Minimum算子的导出转换。|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3QLIT) | 

#### 其他 issue   
难度：:fa-star:  :fa-star:  :fa-star-o: 
|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|6|功能完善类|Add Sqrt Operator Export ONNX：MindSpore模型导出转换成ONNX模型，具体实现细节可拆分为单个MindSpore算子到单个ONNX算子的转换，本任务是实现Sqrt算子的导出转换。|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3QL91?from=project-issue) | 
|7|功能完善类|support python builtin function 'any' in net in graph mode|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3OL8L?from=project-issue) | 
|8|功能完善类|support python builtin function 'all' in net in graph mode|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3OL88) | 
|9|功能完善类 |Modify formula of the Adam optimizer|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3O8W0) | 
|10|功能完善类 |support builtin function sum in net in graph mode|[查看详情](https://gitee.com/mindspore/mindspore/issues/I3OL1I?from=project-issue) |

[查看更多可参与的项目>>](https://gitee.com/gitee-community/gitee-8th-participate-in-open-source)

#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png") 
