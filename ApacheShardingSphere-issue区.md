![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/112714_c89f8f2f_1899542.jpeg "logo.jpeg")  
项目地址：https://gitee.com/Sharding-Sphere/sharding-sphere
#### 活动流程
- 参与 issue 解决 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在issue 列表的新手区/其他 issue 区选择一个或多个 issue 任务，完成它。
2. 完成 issue 描述需求后，在 issue 所属仓库中提交 Pull Request ，并复制 issue ID 至 PR 描述，提交审核（注意：提交 pr 时请备注【Gitee八周年活动】）   
 :point_right: [Fork + PullRequest 模式帮助文档](https://gitee.com/help/categories/42)    
 **注：** 特殊 issue（无需提交至仓库的 issue）请和项目维护者沟通后按指定方式提交。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励 Gitee 大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")      

#### 新手区 issue 
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|1|代码重构|使用Lambda表达式重构For循环| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RFYL) | 
|2|文档新增|新增ShardingSphere-Proxy 配置变更历史的文档| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RDJH) | 
|3|代码重构|移除 Proxy 的枚举 TransactionOperationType| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RDGW) | 
|4|文档优化|根据需求说明完成ShardingSphere-JDBC Spring 命名空间配置变更历史的文档调整| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RDF7) | 
|5|文档优化|根据需求说明完成ShardingSphere-JDBC Spring Boot Starter配置变更历史的文档调整| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RDER) | 
|6|文档优化|根据需求说明完成ShardingSphere-JDBC Java API变更历史的文档优化| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RDCD) | 

#### 其他 issue   
难度：:fa-star:  :fa-star:  :fa-star-o: 
|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|7|新增配置|为每个表提供 queryWithCipherColumn 配置| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3R1DF) | 
|8|代码逻辑优化|目标类的check逻辑里，校验要删除的table是否在BindingTableRules中有引用，如果存在引用，抛出合理异常| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3R4JN) | 
|9|文档优化|将API变更历史迁移到统一目录下，方便用户查找。内容校对，检查各模式下变更历史的正确性，对内容不完整的进行补充。| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RBY2) | 
|10|代码优化|PG Prepare语句解析代码优化| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3R10F) | 
|11|文档优化|参考API配置变更历史文档优化，完成Others目录下文档层级的调整。| [查看详情](https://gitee.com/Sharding-Sphere/sharding-sphere/issues/I3RCT4)| 

[查看更多可参与的项目  >>](https://gitee.com/gitee-community/gitee-8th-participate-in-open-source)

#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png") 