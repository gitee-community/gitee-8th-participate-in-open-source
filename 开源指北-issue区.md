![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/191214_9ad0822b_5694891.png "屏幕截图.png")

项目地址：https://gitee.com/gitee-community/opensource-guide
#### 活动流程
- 参与 issue 解决 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在issue 列表的简单/困难区选择一个或多个 issue 任务，完成它。
2. 完成 issue 描述需求后，在 issue 所属仓库中提交 Pull Request ，并复制 issue ID 至 PR 描述，提交审核（注意：提交 pr 时请备注【Gitee八周年活动】）   
 :point_right: [Fork + PullRequest 模式帮助文档](https://gitee.com/help/categories/42)    
 **注：** 特殊 issue（无需提交至仓库的 issue）请和项目维护者沟通后按指定方式提交。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励 Gitee 大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")      

#### 简单区
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

| 编号 | 类型   | issue 描述                   | issue 链接                                                                                    |
|----|------|----------------------------|---------------------------------------------------------------------------------------------|
| 1  | 内容补充 | 补充更多有关开源误区的内容              | [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2WS) |
| 2  | 内容补充 | 补充「十四五」规划中有关开源的政策表述        | [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2WV) |
| 3  | 内容补充 | 补充商业化开源项目的特征               | [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2WY) |
| 4  | 内容补充 | 补充中国开源商业化案例                | [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2X1) |
| 5  | 内容补充 | 补充商业化项目案例商业化前的背景，商业化后的发展情况 | [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2X3) |


#### 其他区   
难度：:fa-star:  :fa-star:  :fa-star-o: 
|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|6|内容补充|关于 PR 的内容补充| [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2XA) | 
|7|内容补充|从开源许可证角度分析企业开源可能遇到的风险| [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2XD) | 
|8|内容补充|从社区的角度来阐述对核心贡献者的需求| [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2XI) | 
|9|内容补充|参考已有内容，自由提出符合《开源指北》主题的新章节| [查看详情](https://gitee.com/gitee-community/opensource-guide/issues/I3T2XN) | 


[查看更多可参与的项目  >>](https://gitee.com/gitee-community/gitee-8th-participate-in-open-source)

#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png") 