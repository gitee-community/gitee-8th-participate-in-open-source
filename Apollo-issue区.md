![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/203209_58859b26_1899542.png "apollo-logo.png")  
项目地址：[https://gitee.com/ApolloAuto/apollo](https://gitee.com/ApolloAuto/apollo?_from=gitee_search)
#### 活动流程
- 参与 issue 解决 ： 5月27日-6月30日
- 最佳/优秀贡献者评选 ：7月1-2日
- 结果公布+奖品发放：7月5日 

#### 参与方式
1. 在下方 issue 列表的新手区/其他 issue 区选择一个或多个 issue 任务，完成它。
2. 解决问题后，添加 Apollo 开发者社区小助手微信号（apollo_xzs），备注【Gitee 八周年】，添加后发送 gitee ID 以及解决的问题链接，小助手会统一审核。
3. 在本页面下方评论区评论：issue 编号 + 已提交

#### 活动奖励
- 🎖 所有提交 PR 且审核通过的小伙伴都可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **最佳贡献者** ，奖励 500 元京东购物卡一份。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/135812_31e1e48c_1899542.png "京东购物卡-300.png") 
- 🎖 本项目维护者将在活动期间参与 issue 解决的小伙伴中评出 **优秀贡献者** ，奖励周边大礼包一份。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0519/133709_b146a733_1899542.png "gitee 周边大礼包.png")      

#### 新手区 issue 
难度：:fa-star:  :fa-star-o:  :fa-star-o: 

|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|1|参数调试|在自动驾驶仿真时，试图以更高速度进行默认巡航但无法达到该速度，如何才能让汽车达到更高的速度呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1NP)| 
|2|版本兼容|Apollo5.5和6.0都无法和Nvidia agx Xavier以及Ubuntu 18.04同时兼容，有没有哪一个版本的Apollo能和它们同时兼容呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1PM)| 
|3|bug|MSF本地化模块无法正常运行，该问题该如何解决呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1TX)| 
|4|硬件——摄像机|在感知时，Apollo为什么要使用两个6mm相机？如果使用两个相机，如何集成它们的功能呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S23O)| 
|5|bug|感知模块在本地计算机上无法运行，该问题该如何解决呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1V2)| 
|6|单元测试|针对现有Apollo开源代码中的Perception 、PnC、Planning等模块方向，请补全你能发现缺少单元测试的问题，不限数量|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3STKP)|
#### 其他 issue   
难度：:fa-star:  :fa-star:  :fa-star-o: 
|编号|  类型  |  issue 描述 | issue 链接  | 
|---|---|---|---|
|6|bug|我发现在控制模块，小车可能会在接近停止阶段出现加速错误，有人也发现了相同问题吗？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1YT)| 
|7|感知模块——障碍物检测|想知道在感知模块中， LiDAR激光雷达是否会将路边的树叶识别成障碍物？如果是的话，我该如何过滤掉这些树叶呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1ZY)| 
|8|新增功能|1.如何在Apollo和VTD之间搭建一个交换桥梁去进行数据转换呢？；2.如何获得通往Apollo的LGSVL桥的源代码呢？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S21Q)| 
|9|规划模块|1.想请问一下center_line，adc_frenet_l_和offset_to_map这几个参数之间的关系是什么？；2.如何计算横向加速度限制？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S229)| 
|10|文件采集|请问该如何查看仅包含交通灯的记录文件？|[查看详情](https://gitee.com/ApolloAuto/apollo/issues/I3S1X6)|

[查看更多可参与的项目>>](https://gitee.com/gitee-community/gitee-8th-participate-in-open-source)

#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在添加微信时备注【Gitee八周年活动】才可参与活动
- 同一人可以参与解决多个 issue 
- 最佳贡献者和优秀贡献者不重复颁予。
- 关于活动任何问题可添加 Apollo 开发者社区小助手（apollo_xzs）咨询     