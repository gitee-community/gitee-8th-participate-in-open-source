![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/134601_a486ae67_1899542.jpeg "仓库 banner.jpg")    
[八周年活动主会场>>](https://gitee.com/gitee-8th/#/)    
   
近几年，开源技术已逐渐在云计算、大数据、人工智能等领域逐渐发展为技术主流，开发者对于参与优秀开源项目的积极性也逐渐加强。
  
但目前，仍有非常多的开发者在参与开源项目时会因为这些问题困惑：

- 不了解如何选择适合自己参与的开源项目
- 不了解如何参与到感兴趣的开源项目中
- 在众多 issue 任务中，找不到适合自己完成的 issue
  
为了让小伙伴们更快捷地了解并参与到优秀开源项目中，我们联合多个优秀开源社区一起推出了多个易上手的「新手区」issue 任务，大家可以根据项目介绍选择适合自己参与的项目及issue 参与，顺便领个好礼🎁 ～   
   

### 活动时间
5月27日-7月2日

### 活动流程
- 完成 issue 任务： 5月27日 - 6月30日    
- PR 审核及：7月1-2日
- 结果公布+奖品发放：7月5日  


### 活动规则
1、选择合适的项目，完成项目中的任一 issue 任务并获得项目维护者采纳（PR 审核通过）即可获得 Gitee 开源主题贴纸+摄像头遮挡贴一份。  
2、7月1日-2日，每个项目维护者将在活动期间完成 issue 任务的贡献者中评出一个最佳贡献者以及一个积极贡献者，最佳贡献者可获得 500 元京东购物卡一份，积极贡献者可获得Gitee 周边大礼包（含文化衫、马克杯等）一份，本奖项由项目作者评出。    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/150907_e75aaa58_1899542.png "奖品.png") 

### 项目区
以下项目按项目名首字母排列，排名不分先后。
| 项目名  | 项目简介  | 适合参与人员  | 如何参与项目  | issue区 |
|---|---|---|---|---|
| [Apache ShardingSphere](https://gitee.com/Sharding-Sphere/sharding-sphere/)  |具备“平台化、可插拔、云原生”等特点，是大规模数据应用、数据增值应用的理想解决方案。| 扎实的 Java 开发功底,熟悉 Linux 操作系统,熟悉 ShardingSphere  | 参与文档纠错、修改bug、新增功能等  | [点击进入](./ApacheShardingSphere-issue区.md)   |
|[Apollo](https://gitee.com/ApolloAuto/apollo?_from=gitee_search)|Apollo开放平台是一个开放的、完整的、安全的平台，将帮助汽车行业及自动驾驶领域的合作伙伴结合车辆和硬件系统，快速搭建一套属于自己的自动驾驶系统|对百度Apollo比较熟悉，有兴趣继续探索；有一定的技术基础，乐于技术分享及互动|熟悉Apollo开源代码，在不同场景下试用;参与Issue社区互动，回答问题赢奖品|[点击进入](./Apollo-issue区.md)|
| [BootstrapBlazor](https://gitee.com/LongbowEnterprise/BootstrapBlazor)  |一套基于 Bootstrap 和 Blazor 的企业级组件库| 熟悉 asp.net core 熟悉 html css javascript  | 参与文档纠错/修改bug/新增功能 | [点击进入](./BootstrapBlazor-issue区.md)   |
| [DolphinScheduler](https://gitee.com/dolphinscheduler/DolphinScheduler)  |  Apache DolphinScheduler(incubator,原EasyScheduler）是一个分布式工作流任务调度系统，致力于“解决大数据任务之间错综复杂的依赖关系，并监控整个数据处理过程”。DolphinScheduler 以 DAG(有向无环图) 的方式将任务连接起来，可实时监控任务的运行状态，同时支持重试、从指定节点恢复失败、暂停及 Kill任务等操作。 | 熟悉 java 编程语言/熟悉开源，具有一定的抽象思维、基本功底扎实。有一定的开源项目实践经验。代码风格优雅、遵循DolphinScheduler社区规范（编码、PR、文档）等  | 参与文档翻译、视频录制、功能实现等等  | [点击进入](./DolphinScheduler-issue区.md)  |
|[knife4j](https://gitee.com/xiaoym/knife4j)| Knife4j是为Java MVC框架集成Swagger生成Api文档的增强解决方案,小巧,轻量,并且功能强悍!  | 1、熟悉Swagger2以及OpenAPI3的规范；2、熟悉前端JavaScript语言、Vue框架；3、熟悉Java、SpringBoot  | 文档纠错、项目实战总结、修改bug、新增功能特性| [点击进入](./knife4j-issue区.md)  |
| [MindSpore](https://gitee.com/mindspore)  |MindSpore是一款开源的全场景AI计算框架  | 熟悉python与C++开发，以及一定的AI基础知识 | 修改bug、文档纠错、新增功能  | [点击进入](./MindSpore-issue区.md)   |
| [PyMiner](https://gitee.com/py2cn/pyminer)  | 基于Python的可视化数据处理、分析工具，可以通过界面形式操作python代码完成数据任务  |熟悉Python、PyQt、PySide|参与文档纠错，修改bug，新增功能|  [点击进入](./PyMiner-issue区.md)   |
| [RT-Thread](https://gitee.com/rtthread/rt-thread)  | RT-Thread是一个来自中国的开源物联网操作系统，它提供了非常强的可伸缩能力：从一个可以运行在ARM Cortex-M0芯片上的极小内核，到中等的ARM Cortex-M3/4/7系统，甚至是运行于MIPS32、ARM Cortex-A系列处理器上功能丰富系统  | 熟悉 c 编程语言，有一定嵌入式操作系统开发经验  | 参与文档纠错、修改bug | [点击进入](./RT-Thread-issue区.md)   |
| [开源指北](https://gitee.com/gitee-community/opensource-guide)  |【文档类项目】一份给开源新手的保姆级开源百科  |熟悉开源，有一定的开源项目实践经验。没有特别丰富的开源实践经验，但有较强的信息检索与过滤能力，能提供详实的参考资料。| 错别字/标点修改、错误内容更正、内容更新、内容补充（包括新章节设置）| [点击进入](./开源指北-issue区.md) |
  
#### 敲黑板
- 仅在 5月27日-6月30日期间内解决 issue ，且在提交 pr 时备注【Gitee八周年活动】才可参与活动；
- 同一人可以参与解决多个 issue；
- 同一人可以参与多个项目；
- 最佳贡献者和优秀贡献者不重复颁予。
- 不知道如何拉取、提交代码？关于活动任何问题可添加 gitee 小助手（gitee2013）咨询     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png")    

#### 合作社区
![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/202412_bb5adc02_1899542.png "logo.png")
